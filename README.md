# Rental cars interview test

Repository containing test for interview.

Built with
* node v10.15.1
* npm 6.4.1
* create-react-app
* node-sass

## To run
Clone repository and run 

`npm install`
`npm start`

## To build

`npm build`