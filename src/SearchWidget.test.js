import React from 'react';
import ReactDOM from 'react-dom';
import SearchWidget from './SearchWidget';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SearchWidget />, div);
  ReactDOM.unmountComponentAtNode(div);
});
