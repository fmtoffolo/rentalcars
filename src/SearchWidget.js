import React, { Component } from 'react';
import SearchBox from './components/SearchBox/SearchBox';
import './SearchWidget.scss';

class SearchWidget extends Component {
  render() {
    return (
      <div className="App">
        <h2>Where are you going?</h2>
        <SearchBox />
      </div>
    );
  }
}

export default SearchWidget;
