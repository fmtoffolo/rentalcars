import 'react-app-polyfill/ie9';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SearchWidget from './SearchWidget';

ReactDOM.render(<SearchWidget />, document.getElementById('root'));
