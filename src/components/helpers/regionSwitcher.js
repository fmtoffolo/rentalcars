const regionSwitcher = result => {
  switch (result.placeType) {
    case 'A':
      return result.city
        ? `${result.city}, ${result.country}`
        : `${result.country}`;
    case 'C':
      return result.region
        ? `${result.region}, ${result.country}`
        : `${result.country}`;
    case 'D':
      return result.region
        ? `${result.region}, ${result.country}`
        : `${result.country}`;
    case 'T':
      return result.city
        ? `${result.city}, ${result.country}`
        : `${result.country}`;
    default:
      return result.city
        ? `${result.city}, ${result.country}`
        : `${result.country}`;
  }
};

export default regionSwitcher;
