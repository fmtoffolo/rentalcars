const typeMapping = {
  D: 'District',
  C: 'City',
  A: 'Airport',
  P: 'Place',
  T: 'Station'
};

export default typeMapping;
