import React, { Component } from 'react';
import debouncer from '../helpers/debouncer';
import typeMapping from '../helpers/typeMapping';
import regionSwitcher from '../helpers/regionSwitcher';

const Spinner = props => {
  return props.show ? (
    <img
      className={props.className}
      alt="Searching..."
      src="https://cdn2.rcstatic.com/images/site_graphics/newsite/preloader64.gif"
    />
  ) : (
    ''
  );
};

const ResultsList = props => {
  if (!props.show && !props.results.legnth) {
    return '';
  }

  if (!props.results.length) {
    return (
      <ul className="searchbox-results">
        <li className="result">No results found</li>
      </ul>
    );
  }

  return (
    <ul className="searchbox-results" role="listbox" id="results-list">
      {props.results.map(result => {
        return (
          <li
            className="result"
            key={result.index}
            role="option"
            aria-selected={false}
            id={`option-${result.index}`}
          >
            <a className="result-link" href="/">
              <span className={`type-tag tag-${typeMapping[result.placeType]}`}>
                {typeMapping[result.placeType]}
              </span>
              <div className="result-label">
                {result.name}
                <span className="result-label--subtitle">
                  {regionSwitcher(result)}
                </span>
              </div>
              {result.popular ? (
                <span className="is-popular">Popular</span>
              ) : (
                ''
              )}
            </a>
          </li>
        );
      })}
    </ul>
  );
};

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: '',
      results: [],
      searching: false,
      gotResults: false,
      active: false
    };

    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleOnFocus = this.handleOnFocus.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);

    //we create a debounced version of the makeAPICall function
    this.handleGetResponse = debouncer(async function makeAPICall(
      searchValue,
      limit
    ) {
      let request;
      try {
        request = await fetch(
          `https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=${limit ||
            6}&solrTerm=${searchValue}`
        );
        request = await request.json();
      } catch (error) {
        //error handling for API calls
        this.setState({
          results: [],
          searching: false,
          gotResults: true,
          active: true
        });
        return;
      }

      //here we catch errant requests...and set state accordingly
      if (this.state.searchValue.length < 2) {
        this.setState({
          results: [],
          searching: false,
          gotResults: false,
          active: this.state.active
        });
        return;
      }

      if (request.results.numFound > 0) {
        this.setState({
          results: request.results.docs,
          searching: false,
          gotResults: true,
          active: true
        });
      } else {
        this.setState({
          results: [],
          searching: false,
          gotResults: true,
          active: true
        });
      }
    },
    this.props.debounce || 500);
  }

  async handleOnChange(event) {
    await this.setState({ searchValue: event.target.value, gotResults: false });

    //we return if search value too short
    if (this.state.searchValue.length <= 1) {
      this.setState({ results: [], searching: false });
      return;
    }

    this.setState({ searching: true });

    //we call the debounced API caller
    this.handleGetResponse(this.state.searchValue);
  }

  handleOnBlur(event) {
    this.setState({ active: false });
  }

  handleOnFocus(event) {
    this.setState({ active: true });
  }

  render() {
    return (
      <div className="searchbox-component">
        <label htmlFor="search" className="searchbox-label">
          Pick-up Location
        </label>

        <div className="searchbox-container">
          <input
            id="search"
            className="searchbox"
            type="text"
            value={this.state.searchValue}
            onChange={this.handleOnChange}
            placeholder="city, airport, station,
            region and district..."
            onBlur={this.handleOnBlur}
            onFocus={this.handleOnFocus}
            aria-required="true"
            aria-autocomplete="list"
            aria-controls="results-list"
            aria-expanded={this.state.gotResults && this.state.active}
            role="combobox"
          />
          <Spinner className="spinner" show={this.state.searching} />
        </div>
        <ResultsList
          show={this.state.gotResults && this.state.active}
          results={this.state.results}
        />
      </div>
    );
  }
}

export default SearchBox;
